# ui-testing-module
--------------------

A wrapper around the webdriverio framework, helping with quick setup, and minimal coding by reducing the required work done to: 

1. Defining your pages (pages)
2. What you want to do with your pages (features)

# Quickstart

1. Install this module into the root directory of your project

2. Create a test folder dir in your project with the following folder structure
``` 
- test
  |- pages
  |- specs
```
3. Add the following ```config.json``` file into the ```./test``` directory

```
{
  "testDirPath": "./test",
  "seleniumStandalone": true,
  "maxInstances": "1",
  "logLevel": "trace",
  "baseUrl": "<Your baseUrl>" // i.e. "https://webdriver.io",
  "capabilities": [
    {
      "browserName": "chrome",
      "goog:chromeOptions": {
        "args": ["--headless", "--disable-gpu", "--disable-web-security"]
      },
      "maxInstances": 1
    }
  ],
  "browserTimeouts": 5000,
  "specs": [
    <absolute path to your spec directory>
    // i.e. "~/test/specs/**/*.feature"
  ],
  "stepDefinitions": [
    <absolute path to your stepDefinitions directory>
    ~/test/step_definitions/**/*.js"
  ],
  "relativePageFolder": "./test/pages"
}
```
4. Add the following page object in the ```./pages``` folder

```
module.exports = {
    'get started header': '//h1[@class="postHeaderTitle"]'
};
```

5. Add the following page object in the ```./specs``` folder

```
Feature: Initial tests
  Sample steps as an example

  Scenario: Navigate to wdio's getting started page and see its title
    Given I navigate to the route "/"
    When I land on the "wdio" page
    Then I see the element "search input"
```

6. From the root directory of your project, run ```ui-testing-module ./test/config.json```

# TODOs
1. Refactor cli generator to use yargs?
2. Allow for dynamic, configurable file structures
3. Default file path (.test/e2e/ui?)
4. Introduce concurrent runs
5. Set up safaridriver: https://www.npmjs.com/package/wdio-safaridriver-service
6. Documentation should include process.cwd documentation


