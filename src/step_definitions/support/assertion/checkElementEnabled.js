export default (elementLocator, checkIfNotEnabled) => {
    const checkIfIsDisabled = checkIfNotEnabled ? true : false;
    $(currentPage[elementLocator]).waitForEnabled(undefined, checkIfIsDisabled);
}