import fs from "fs";
import inquirer from "inquirer";
import path from "path";
import loggerModule from "./loggerModule";

const logger = loggerModule.getLogger();

export default () => {
  logger.info(`cli-config:
===============================================
UI Test Infrastructure Configuration Helper
===============================================
`);

  inquirer
    .prompt([
      {
        default: "./",
        message: "Please input the path to your 'test' folder.",
        name: "testDirPath",
        type: "input"
      },
      {
        default: true,
        message: "Are you using selenium-standalone?",
        name: "seleniumStandalone",
        type: "confirm"
      },
      {
        default: "localhost",
        message: "The selenium server host address",
        name: "host",
        type: "input",
        when: answers => !answers.seleniumStandalone
      },
      {
        default: "4444",
        message: "The selenium server port",
        name: "port",
        type: "input",
        when: answers => !answers.seleniumStandalone
      },
      {
        default: 5,
        message: "Maximum number of concurrently running instances.",
        name: "maxInstances",
        type: "input"
      },
      {
        choices: ["trace", "debug", "info", "warn", "error", "silent"],
        default: "silent",
        message: "Level of logging verbosity",
        name: "logLevel",
        type: "list"
      },
      {
        default: "http://localhost",
        message: "What is the base url?",
        name: "baseUrl",
        type: "input"
      },
      {
        message: "Path to file with custom steps for test features (optional)",
        name: "customSteps",
        type: "input"
      },
      {
        choices: ["chrome", "firefox", "phantomjs", "safari"],
        message: "What capabilities do you want to have?",
        name: "capabilities",
        type: "checkbox"
      },
      {
        default: 3,
        message: "What are the maximum concurrent instances per capability?",
        name: "maxInstancesPerCapability",
        type: "input",
        when: answers => answers.capabilities.length > 0
      },
      {
        default: 5000,
        message: "Time for runner to wait for an element assertion before failing a test",
        name: "browserTimeouts",
        type: "input"
      },
      {
        default: false,
        message: "Would you like to create an empty page object file?",
        name: "createPageObject",
        type: "confirm"
      },
      {
        default: false,
        message: "Would you like to create an example feature file?",
        name: "createFeatureFile",
        type: "confirm"
      }
    ])
    .then(answers => {
      // object that will store the user config
      const configObj = {};

      // optional answers
      if (answers.customSteps) {
        configObj.customSteps = [answers.customSteps];
      }

      // store answers in config object
      Object.keys(answers).forEach(key => {
        if (answers[key] && !configObj[key]) {
          configObj[key] = answers[key];
        }
      });

      configObj.pageObjectPath = path.resolve(answers.testDirPath, "./functional/page-objects/");
      configObj.specs = [path.resolve(answers.testDirPath, "./specs/") + "/*.feature"];
      delete configObj.createTestDir;
      if (answers.capabilities.length > 0) {
        configObj.capabilities = [];
        answers.capabilities.forEach((capability, i) => {
          configObj.capabilities[i] = {
            browserName: capability,
            maxInstances: answers.maxInstancesPerCapability
          };
        });
      } else {
        delete configObj.capabilities;
      }
      // fs.mkdirSync(path.resolve(answers.testDirPath, "./functional"));
      // fs.mkdirSync(path.resolve(answers.testDirPath, "./functional/page-objects"));
      // fs.mkdirSync(path.resolve(answers.testDirPath, "./functional/features"));

      // create config file
      fs.writeFileSync(path.resolve(answers.testDirPath, "./config.json"), JSON.stringify(configObj, null, 2), "utf8");

      if (answers.createPageObject) {
        fs.writeFileSync(
          path.resolve(answers.testDirPath, "./functional/page-objects/myPage.page.js"),
          templates.pageObj,
          "utf8"
        );
      }
      if (answers.createFeatureFile) {
        fs.writeFileSync(
          path.resolve(answers.testDirPath, "./functional/features/testFeature.feature"),
          templates.feature,
          "utf8"
        );
      }

      logger.info(`cli-config:
Configuration file was created successfully!
To run your tests, execute:

$ ui-testing-module
`);
      process.exit(0);
    });
};
