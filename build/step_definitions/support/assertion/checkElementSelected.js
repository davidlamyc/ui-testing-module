"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _default = function _default(elementLocator, notSelectedString) {
  var checkIfNotSelected = notSelectedString ? true : false;
  var isSelected = $(currentPage[elementLocator]).isSelected();

  if (checkIfNotSelected) {
    if (isSelected === true) {
      throw new Error("Element ".concat(elementLocator, " is selected when it should not be selected."));
    }
  } else {
    if (isSelected === false) {
      throw new Error("Element ".concat(elementLocator, " is not selected when it should be selected."));
    }
  }
};

exports["default"] = _default;