"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _default = function _default(elementLocator, text) {
  var actualText = $(currentPage[elementLocator]).getText();

  if (actualText !== text) {
    throw new Error("Expected text: ".concat(text, ", Actual text: ").concat(actualText));
  }
};

exports["default"] = _default;