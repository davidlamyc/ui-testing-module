"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _default = function _default(elementLocator, checkIfNotEnabled) {
  var checkIfIsDisabled = checkIfNotEnabled ? true : false;
  $(currentPage[elementLocator]).waitForEnabled(undefined, checkIfIsDisabled);
};

exports["default"] = _default;