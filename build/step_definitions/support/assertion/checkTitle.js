"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _default = function _default(title) {
  var actualTitle = browser.getTitle();

  if (actualTitle !== title) {
    throw new Error("Expected title: ".concat(title, ", Actual title: ").concat(actualTitle));
  }
};

exports["default"] = _default;