"use strict";

module.exports = function (element, exactly) {
  var nrOfElements = $$(currentPage[element]);
  var actualNumberOfElements = nrOfElements.length;
  var expectedNumberOfElement = parseInt(exactly);

  if (actualNumberOfElements !== expectedNumberOfElement) {
    throw new Error("Element with selector \"".concat(element, "\" should exist exactly ").concat(expectedNumberOfElement, " time(s), but appears ").concat(actualNumberOfElements, " time(s)"));
  }
};