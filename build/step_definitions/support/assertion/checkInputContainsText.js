"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _default = function _default(elementLocator, text) {
  var actualText = $(currentPage[elementLocator]).getValue();

  if (actualText.indexOf(text) == -1) {
    throw new Error("The element's text \"".concat(actualText, "\" does not contain \"").concat(text, "\""));
  }
};

exports["default"] = _default;