"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _default = function _default(isCSS, attrName, elementLocator, falseCase, expectedValue) {
  $(currentPage[elementLocator]).waitForDisplayed();
  var command = isCSS ? "getCSSProperty" : "getAttribute";
  var attrType = isCSS ? "CSS attribute" : "Attribute";
  var attributeValue = $(currentPage[elementLocator])[command](attrName);

  if (attrName.match(/(color|font-weight)/)) {
    attributeValue = attributeValue.value;
  }

  if (falseCase) {
    if (attributeValue === expectedValue) {
      throw new Error("".concat(attrType, " of element \"").concat(elementLocator, "\" should not contain ") + "\"".concat(attributeValue, "\""));
    }
  } else {
    if (attributeValue !== expectedValue) {
      throw new Error("".concat(attrType, " of element \"").concat(elementLocator, "\" should not contain ") + "\"".concat(attributeValue, "\", but \"").concat(expectedValue, "\""));
    }
  }
};

exports["default"] = _default;