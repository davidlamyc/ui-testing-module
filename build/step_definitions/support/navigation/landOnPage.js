"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _path = _interopRequireDefault(require("path"));

var _process = _interopRequireDefault(require("process"));

var _glob = _interopRequireDefault(require("glob"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = function _default(page) {
  var relativePageFolderPath = _path["default"].join(browser.config.relativePageFolderPath, "/**/".concat(page, ".js"));

  var currentPage = require(_glob["default"].sync(relativePageFolderPath)[0]);

  browser.setPage(currentPage);
};

exports["default"] = _default;