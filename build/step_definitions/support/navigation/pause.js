"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _default = function _default(pause) {
  browser.pause(pause);
};

exports["default"] = _default;