"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _default = function _default(urlPath) {
  browser.url(browser.options.baseUrl + urlPath);
};

exports["default"] = _default;