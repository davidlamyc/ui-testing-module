"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _default = function _default(type, page) {
  var url = type === "url" ? page : browser.options.baseUrl + page;
  browser.url(url);
};

exports["default"] = _default;