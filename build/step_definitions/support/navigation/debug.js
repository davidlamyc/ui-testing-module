"use strict";

// Documentation: https://v5.webdriver.io/docs/api/browser/debug.html
module.exports = function () {
  browser.debug();
};