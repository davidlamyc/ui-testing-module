"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _default = function _default(elementLocator, text) {
  $(currentPage[elementLocator]).waitForDisplayed();
  $(currentPage[elementLocator]).setValue(text);
};

exports["default"] = _default;