"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _default = function _default(text, elementLocator) {
  $(currentPage[elementLocator]).waitForDisplayed();
  $(currentPage[elementLocator]).selectByVisibleText(text);
};

exports["default"] = _default;