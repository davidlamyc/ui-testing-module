"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _default = function _default(clickOrDoubleClick, elementLocator) {
  $(currentPage[elementLocator]).waitForDisplayed();

  if (clickOrDoubleClick === 'doubleclick') {
    $(currentPage[elementLocator]).doubleClick();
  } else {
    $(currentPage[elementLocator]).click();
  }
};

exports["default"] = _default;