"use strict";

var _cucumber = require("cucumber");

var _openWebsite = _interopRequireDefault(require("./support/navigation/openWebsite"));

var _resizeScreenSize = _interopRequireDefault(require("./support/navigation/resizeScreenSize"));

var _goToUrl = _interopRequireDefault(require("./support/navigation/goToUrl"));

var _pause = _interopRequireDefault(require("./support/navigation/pause"));

var _landOnPage = _interopRequireDefault(require("./support/navigation/landOnPage"));

var _debug = _interopRequireDefault(require("./support/navigation/debug"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

(0, _cucumber.Given)(/^I open the (url|site) "([^"]*)?"$/, _openWebsite["default"]);
(0, _cucumber.Given)(/^I have a screen that is ([\d]+) by ([\d]+) pixels$/, _resizeScreenSize["default"]);
(0, _cucumber.Given)(/^I go to url "([^"]*)?"$/, _goToUrl["default"]);
(0, _cucumber.When)(/^I land on the "([^"]*)?" page$/, _landOnPage["default"]);
(0, _cucumber.Then)(/^I pause for (\d+)ms$/, _pause["default"]);
(0, _cucumber.Given)("I debug", _debug["default"]);