"use strict";

var _cucumber = require("cucumber");

var _clickElement = _interopRequireDefault(require("./support/interaction/clickElement"));

var _selectInputOption = _interopRequireDefault(require("./support/interaction/selectInputOption"));

var _pressEnter = _interopRequireDefault(require("./support/interaction/pressEnter"));

var _fillInput = _interopRequireDefault(require("./support/interaction/fillInput"));

var _acceptAlert = _interopRequireDefault(require("./support/interaction/acceptAlert"));

var _scrollToElement = _interopRequireDefault(require("./support/interaction/scrollToElement"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

(0, _cucumber.Then)(/^I (click|doubleclick) on the element "([^"]*)?"$/, _clickElement["default"]);
(0, _cucumber.Then)(/^I select the option "([^"]*)?" from the select input "([^"]*)?"$/, _selectInputOption["default"]);
(0, _cucumber.Then)(/^I press enter$/, _pressEnter["default"]);
(0, _cucumber.Then)(/^I fill the input "([^"]*)?" with text "([^"]*)?"$/, _fillInput["default"]);
(0, _cucumber.Then)(/^I accept the alert box$/, _acceptAlert["default"]);
(0, _cucumber.Then)(/^I scroll to the element "([^"]*)?"$/, _scrollToElement["default"]);