"use strict";

var _cucumber = require("cucumber");

var _checkElementVisibility = _interopRequireDefault(require("./support/assertion/checkElementVisibility"));

var _checkElementEnabled = _interopRequireDefault(require("./support/assertion/checkElementEnabled"));

var _checkElementSelected = _interopRequireDefault(require("./support/assertion/checkElementSelected"));

var _checkTitle = _interopRequireDefault(require("./support/assertion/checkTitle"));

var _checkElementHasText = _interopRequireDefault(require("./support/assertion/checkElementHasText"));

var _checkElementContainsText = _interopRequireDefault(require("./support/assertion/checkElementContainsText"));

var _checkInputHasText = _interopRequireDefault(require("./support/assertion/checkInputHasText"));

var _checkInputContainsText = _interopRequireDefault(require("./support/assertion/checkInputContainsText"));

var _checkProperty = _interopRequireDefault(require("./support/assertion/checkProperty"));

var _checkElementCount = _interopRequireDefault(require("./support/assertion/checkElementCount"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

(0, _cucumber.Then)(/^I( do not)* see the element "([^"]*)?"$/, _checkElementVisibility["default"]);
(0, _cucumber.Then)(/^I check that the element "([^"]*)?" is( not)* enabled$/, _checkElementEnabled["default"]);
(0, _cucumber.Then)(/^I check that the element "([^"]*)?" is( not)* selected$/, _checkElementSelected["default"]);
(0, _cucumber.Then)(/^I check that the title is "([^"]*)?"$/, _checkTitle["default"]);
(0, _cucumber.Then)(/^I check that the element "([^"]*)?" has the text "([^"]*)?"$/, _checkElementHasText["default"]);
(0, _cucumber.Then)(/^I check that the element "([^"]*)?" contains the text "([^"]*)?"$/, _checkElementContainsText["default"]);
(0, _cucumber.Then)(/^I check that the input "([^"]*)?" has the text "([^"]*)?"$/, _checkInputHasText["default"]);
(0, _cucumber.Then)(/^I check that the input "([^"]*)?" contains the text "([^"]*)?"$/, _checkInputContainsText["default"]);
(0, _cucumber.Then)(/^I check that the( css)* attribute "([^"]*)?" from element "([^"]*)?" is( not)* "([^"]*)?"$/, _checkProperty["default"]);
(0, _cucumber.Then)(/^I check that the element "([^"]*)?" appears exactly ([\d]+) times$/, _checkElementCount["default"]);