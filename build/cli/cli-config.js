"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _fs = _interopRequireDefault(require("fs"));

var _inquirer = _interopRequireDefault(require("inquirer"));

var _path = _interopRequireDefault(require("path"));

var _loggerModule = _interopRequireDefault(require("./loggerModule"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var logger = _loggerModule["default"].getLogger();

var _default = function _default() {
  logger.info("cli-config:\n===============================================\nUI Test Infrastructure Configuration Helper\n===============================================\n");

  _inquirer["default"].prompt([{
    "default": "./",
    message: "Please input the path to your 'test' folder.",
    name: "testDirPath",
    type: "input"
  }, {
    "default": true,
    message: "Are you using selenium-standalone?",
    name: "seleniumStandalone",
    type: "confirm"
  }, {
    "default": "localhost",
    message: "The selenium server host address",
    name: "host",
    type: "input",
    when: function when(answers) {
      return !answers.seleniumStandalone;
    }
  }, {
    "default": "4444",
    message: "The selenium server port",
    name: "port",
    type: "input",
    when: function when(answers) {
      return !answers.seleniumStandalone;
    }
  }, {
    "default": 5,
    message: "Maximum number of concurrently running instances.",
    name: "maxInstances",
    type: "input"
  }, {
    choices: ["trace", "debug", "info", "warn", "error", "silent"],
    "default": "silent",
    message: "Level of logging verbosity",
    name: "logLevel",
    type: "list"
  }, {
    "default": "http://localhost",
    message: "What is the base url?",
    name: "baseUrl",
    type: "input"
  }, {
    message: "Path to file with custom steps for test features (optional)",
    name: "customSteps",
    type: "input"
  }, {
    choices: ["chrome", "firefox", "phantomjs", "safari"],
    message: "What capabilities do you want to have?",
    name: "capabilities",
    type: "checkbox"
  }, {
    "default": 3,
    message: "What are the maximum concurrent instances per capability?",
    name: "maxInstancesPerCapability",
    type: "input",
    when: function when(answers) {
      return answers.capabilities.length > 0;
    }
  }, {
    "default": 5000,
    message: "Time for runner to wait for an element assertion before failing a test",
    name: "browserTimeouts",
    type: "input"
  }, {
    "default": false,
    message: "Would you like to create an empty page object file?",
    name: "createPageObject",
    type: "confirm"
  }, {
    "default": false,
    message: "Would you like to create an example feature file?",
    name: "createFeatureFile",
    type: "confirm"
  }]).then(function (answers) {
    // object that will store the user config
    var configObj = {}; // optional answers

    if (answers.customSteps) {
      configObj.customSteps = [answers.customSteps];
    } // store answers in config object


    Object.keys(answers).forEach(function (key) {
      if (answers[key] && !configObj[key]) {
        configObj[key] = answers[key];
      }
    });
    configObj.pageObjectPath = _path["default"].resolve(answers.testDirPath, "./functional/page-objects/");
    configObj.specs = [_path["default"].resolve(answers.testDirPath, "./specs/") + "/*.feature"];
    delete configObj.createTestDir;

    if (answers.capabilities.length > 0) {
      configObj.capabilities = [];
      answers.capabilities.forEach(function (capability, i) {
        configObj.capabilities[i] = {
          browserName: capability,
          maxInstances: answers.maxInstancesPerCapability
        };
      });
    } else {
      delete configObj.capabilities;
    } // fs.mkdirSync(path.resolve(answers.testDirPath, "./functional"));
    // fs.mkdirSync(path.resolve(answers.testDirPath, "./functional/page-objects"));
    // fs.mkdirSync(path.resolve(answers.testDirPath, "./functional/features"));
    // create config file


    _fs["default"].writeFileSync(_path["default"].resolve(answers.testDirPath, "./config.json"), JSON.stringify(configObj, null, 2), "utf8");

    if (answers.createPageObject) {
      _fs["default"].writeFileSync(_path["default"].resolve(answers.testDirPath, "./functional/page-objects/myPage.page.js"), templates.pageObj, "utf8");
    }

    if (answers.createFeatureFile) {
      _fs["default"].writeFileSync(_path["default"].resolve(answers.testDirPath, "./functional/features/testFeature.feature"), templates.feature, "utf8");
    }

    logger.info("cli-config:\nConfiguration file was created successfully!\nTo run your tests, execute:\n\n$ ui-testing-module\n");
    process.exit(0);
  });
};

exports["default"] = _default;