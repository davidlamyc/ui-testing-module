"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = createWdioConfig;

var _fs = _interopRequireDefault(require("fs"));

var _glob = _interopRequireDefault(require("glob"));

var _path = _interopRequireDefault(require("path"));

var _loggerModule = _interopRequireDefault(require("./loggerModule"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var logger = _loggerModule["default"].getLogger();

function createWdioConfig(config, configDir, wdioConfigFile) {
  // Server config
  var serverConfig = config.user ? "" : "\n  hostname: \"".concat(config.host, "\",\n  port: ").concat(config.port, ",\n  path: \"").concat(config.path, "\",\n  ");
  var serviceProviders = !config.user ? "" : "\n  user: \"".concat(config.user, "\",\n  key: \"").concat(config.key, "\",\n  ");
  /*
   * Feature Files:
   * Take 'specs' array passed from project level config file
   * containing glob patterns to feature files
   */

  var specPaths = [];
  config.specs.forEach(function (spec) {
    var specGlob = _path["default"].resolve(process.cwd(), spec);

    var specs = _glob["default"].sync(specGlob);

    specs.forEach(function (spec) {
      return specPaths.push(JSON.stringify(spec));
    });
  });

  if (specPaths.length === 0) {
    throw new Error("Specs not found. See warnings.");
  }
  /*
   * Capabilities
   */
  // Handle configuration properties specific to browser drivers
  // i.e. 'goog:chromeOptions' for chromedriver


  var browserOptions;

  switch (config.capabilities[0].browserName) {
    case 'chrome':
      browserOptions = "\n      'goog:chromeOptions': { \n        args: ".concat(JSON.stringify(config.capabilities[0]["goog:chromeOptions"].args), ",\n      }\n      ");
      break;

    case 'safari':
      browserOptions = "";
      break;

    case 'firefox':
      browserOptions = "";
      break;

    default:
      logger.error("create-wdio-config: browserName not valid");
      break;
  }

  var capabilities = "\n    capabilities: [\n      {\n        maxInstances: ".concat(config.capabilities[0].maxInstances, ", \n        browserName: '").concat(config.capabilities[0].browserName, "',\n        ").concat(browserOptions, "\n      }\n    ]\n  "); // Services

  var services;

  switch (config.capabilities[0].browserName) {
    case 'chrome':
      services = "services: ['chromedriver'],";
      break;

    case 'safari':
      services = "services: ['safaridriver'],";
      break;

    case 'firefox':
      services = "services: ['geckodriver'],";
      break;

    default:
      logger.error("create-wdio-config: browserName not valid");
  } // Reporters


  var reporters = "\n  reporters: [\n    [\"allure\", {\n      disableWebdriverStepsReporting: ".concat(config.allure ? config.allure.disableWebdriverStepsReporting ? config.allure.disableWebdriverStepsReporting : false : false, ",\n      disableWebdriverScreenshotsReporting: ").concat(config.allure ? config.allure.disableWebdriverScreenshotsReporting ? config.allure.disableWebdriverScreenshotsReporting : false : false, ",\n      useCucumberStepReporter: true,\n      outputDir: ").concat(JSON.stringify(_path["default"].resolve(configDir, "".concat(config.reportOutDir, "/allure-results/"))), "\n    }],\n    [\"junit\", {\n      outputDir: ").concat(JSON.stringify(_path["default"].resolve(configDir, "".concat(config.reportOutDir, "/junit-results/"))), "\n    }],\n    \"spec\",\n    ").concat(config.reportportal ? "[reportportal, ".concat(JSON.stringify(config.reportportal), "],") : "", "\n  ],\n  ");
  /*
   * Step definitions:
   * Take 'step_definitions' array passed from project level config file
   * containing glob patterns to  files
   */

  var stepPaths = []; // Framework predefined steps

  var stepDir = _path["default"].resolve(__dirname, "../step_definitions/");

  _fs["default"].readdirSync(stepDir).filter(function (f) {
    return f.endsWith(".js");
  }).forEach(function (f) {
    return stepPaths.push(JSON.stringify(_path["default"].resolve(stepDir, f)));
  }); // Project custom steps


  config.stepDefinitions.forEach(function (step) {
    var stepDefinitionsGlob = _path["default"].resolve(process.cwd(), step);

    _glob["default"].sync(stepDefinitionsGlob).forEach(function (x) {
      return stepPaths.push(JSON.stringify(_path["default"].resolve(x)));
    });
  });
  /*
   * Pages (relativePagePath):
   * Take 'pages' array passed from project level config file
   * containing glob patterns to files
   */

  var relativePageFolderPath = "relativePageFolderPath: '".concat(_path["default"].join(process.cwd(), config.pages), "'");
  logger.info("create-wdio-config: start creating wdio config object");
  var fileOut = "\nconst path = require(\"path\");\nconst url = require(\"url\");\nconst fs = require(\"fs\")\n\n// See https://webdriver.io/docs/configurationfile.html\nexports.config = {\n  // ====================\n  // Custom Options\n  // ====================\n  //\n  currentPage: {},\n  ".concat(relativePageFolderPath, ",\n  //\n  // ====================\n  // Runner Configuration\n  // ====================\n  //\n  // WebdriverIO allows it to run your tests in arbitrary locations (e.g. locally or\n  // on a remote machine).\n  runner: \"local\",\n  //\n  // =====================\n  // Server Configurations\n  // =====================\n  // Host address of the running Selenium server. This information is usually obsolete as\n  // WebdriverIO automatically connects to localhost. Also, if you are using one of the\n  // supported cloud services like Sauce Labs, Browserstack, or Testing Bot you don't\n  // need to define host and port information because WebdriverIO can figure that out\n  // according to your user and key information. However, if you are using a private Selenium\n  // backend you should define the host address, port, and path here.\n  ").concat(serverConfig, "\n  //\n  // =================\n  // Service Providers\n  // =================\n  // WebdriverIO supports Sauce Labs, Browserstack, and Testing Bot (other cloud providers\n  // should work too though). These services define specific user and key (or access key)\n  // values you need to put in here in order to connect to these services.\n  ").concat(serviceProviders, "\n  //\n  // If you run your tests on SauceLabs you can specify the region you want to run your tests\n  // in via the 'region' property. Available short handles for regions are 'us' (default) and 'eu'.\n  // These regions are used for the Sauce Labs VM cloud and the Sauce Labs Real Device Cloud.\n  // If you don't provide the region it will default for the 'us'\n\n  //\n  // ==================\n  // Specify Test Files\n  // ==================\n  // Define which test specs should run. The pattern is relative to the directory\n  // from which 'wdio' was called. Notice that, if you are calling 'wdio' from an\n  // NPM script (see https://docs.npmjs.com/cli/run-script) then the current working\n  // directory is where your package.json resides, so 'wdio' will be called from there.\n  //\n  specs: [").concat(specPaths.map(function (x) {
    return "\n    ".concat(x);
  }).join(","), "]\n  ,\n  // Patterns to exclude.\n  exclude: [],\n  //\n  // ============\n  // Capabilities\n  // ============\n  // Define your capabilities here. WebdriverIO can run multiple capabilities at the same\n  // time. Depending on the number of capabilities, WebdriverIO launches several test\n  // sessions. Within your capabilities you can overwrite the spec and exclude options in\n  // order to group specific specs to a specific capability.\n  //\n  // First, you can define how many instances should be started at the same time. Let's\n  // say you have 3 different capabilities (Chrome, Firefox, and Safari) and you have\n  // set maxInstances to 1; wdio will spawn 3 processes. Therefore, if you have 10 spec\n  // files and you set maxInstances to 10, all spec files will get tested at the same time\n  // and 30 processes will get spawned. The property handles how many capabilities\n  // from the same test should run tests.\n  //\n  maxInstances: ").concat(config.maxInstances, ",\n  //\n  // If you have trouble getting all important capabilities together, check out the\n  // Sauce Labs platform configurator - a great tool to configure your capabilities:\n  // https://docs.saucelabs.com/reference/platforms-configurator\n  //\n  // maxInstances can get overwritten per capability. So if you have an in-house Selenium\n  // grid with only 5 firefox instances available you can make sure that not more than\n  // 5 instances get started at a time.\n  //\n  // If outputDir is provided WebdriverIO can capture driver session logs\n  // it is possible to configure which logTypes to include/exclude.\n  // excludeDriverLogs: ['*'], // pass '*' to exclude all driver session logs\n  // excludeDriverLogs: ['bugreport', 'server'],\n  ").concat(capabilities, ",\n  path: '/',\n  //\n  // ===================\n  // Test Configurations\n  // ===================\n  // Define all options that are relevant for the WebdriverIO instance here\n  //\n  // By default WebdriverIO commands are executed in a synchronous way using\n  // the wdio-sync package. If you still want to run your tests in an async way\n  // e.g. using promises you can set the sync option to false.\n  sync: true,\n  //\n  // Level of logging verbosity: trace | debug | info | warn | error | silent\n  logLevel: \"").concat(config.logLevel, "\",\n  //\n  // Enables colors for log output.\n  coloredLogs: true,\n  //\n  // Set specific log levels per logger\n  // loggers:\n  // - webdriver, webdriverio\n  // - @wdio/applitools-service, @wdio/browserstack-service, @wdio/devtools-service, @wdio/sauce-service\n  // - @wdio/mocha-framework, @wdio/jasmine-framework\n  // - @wdio/local-runner, @wdio/lambda-runner\n  // - @wdio/sumologic-reporter\n  // - @wdio/cli, @wdio/config, @wdio/sync, @wdio/utils\n  // Level of logging verbosity: trace | debug | info | warn | error | silent\n  // logLevels: {\n  //     webdriver: 'info',\n  //     '@wdio/applitools-service': 'info'\n  // },\n  //\n  // If you only want to run your tests until a specific amount of tests have failed use\n  // bail (default is 0 - don't bail, run all tests).\n  bail: 0,\n  //\n  // Saves a screenshot to a given path if a command fails.\n  screenshotPath: ").concat(JSON.stringify(_path["default"].resolve(configDir, config.errorshotsOutDir)), ",\n  //\n  // Set a base URL in order to shorten url command calls. If your 'url' parameter starts\n  // with '/', the base url gets prepended, not including the path portion of your baseUrl.\n  // If your 'url' parameter starts without a scheme or '/' (like 'some/path'), the base url\n  // gets prepended directly.\n  baseUrl: \"").concat(config.baseUrl, "\",\n  //\n  // Default timeout for all waitFor* commands.\n  waitforTimeout: ").concat(config.browserTimeouts, ",\n  //\n  // Default timeout in milliseconds for request\n  // if Selenium Grid doesn't send response\n  connectionRetryTimeout: 90000,\n  //\n  // Default request retries count\n  connectionRetryCount: 3,\n  //\n  // Plugins\n  // Initialize the browser instance with a WebdriverIO plugin. The object should have the\n  // plugin name as key and the desired plugin options as property. Make sure you have\n  // the plugin installed before running any tests.\n  //\n  // Test runner services\n  // Services take over a specific job you don't want to take care of. They enhance\n  // your test setup with almost no effort. Unlike plugins, they don't add new\n  // commands. Instead, they hook themselves up into the test process.\n  ").concat(services, "\n\n  // Framework you want to run your specs with.\n  // The following are supported: Mocha, Jasmine, and Cucumber\n  // see also: https://webdriver.io/docs/frameworks.html\n  //\n  // Make sure you have the wdio adapter package for the specific framework installed\n  // before running any tests.\n  framework: \"cucumber\",\n  //\n  // The number of times to retry the entire specfile when it fails as a whole\n  // specFileRetries: 1,\n  //\n  // Test reporter for stdout.\n  // The only one supported by default is 'dot'\n  // see also: https://webdriver.io/docs/dot-reporter.html\n  ").concat(reporters, "\n  //\n  // If you are using Cucumber you need to specify the location of your step definitions.\n  // See also: https://github.com/webdriverio/webdriverio/tree/master/packages/wdio-cucumber-framework#cucumberopts-options\n  cucumberOpts: {\n    // <string[]> (file/dir) require files before executing features\n    require: [").concat(stepPaths.map(function (x) {
    return "\n      ".concat(x);
  }).join(","), "],\n    // <string[]> module used for processing required features\n    requireModule: [\"@babel/register\"],\n    // <boolean> show full backtrace for errors\n    backtrace: false,\n    // <boolean> disable colors in formatter output\n    colors: false,\n    // <boolean> invoke formatters without executing steps\n    dryRun: false,\n    // <boolean> abort the run on first failure\n    failFast: false,\n    // <string[]> (type[:path]) specify the output format, optionally supply PATH to redirect formatter output (repeatable)\n    format: [\"pretty\"],\n    // <boolean> Enable this config to treat undefined definitions as warnings.\n    ignoreUndefinedDefinitions: false,\n    // <string[]> (name) specify the profile to use\n    profile: [],\n    // <boolean> hide step definition snippets for pending steps\n    snippets: true,\n    // <boolean> hide source uris\n    source: true,\n    // <boolean> fail if there are any undefined or pending steps\n    strict: true,\n    // <string> (expression) only execute the features or scenarios with\n    // tags matching the expression, see https://docs.cucumber.io/tag-expressions/\n    tagExpression: \"").concat(config.tags, "\",\n    // <boolean> add cucumber tags to feature or scenario name\n    tagsInTitle: false,\n    // <number> timeout for step definitions\n    timeout: ").concat(config.cucumberOpts.timeout, ",\n  },\n\n  // Custom options\n  customOpts: ").concat(JSON.stringify(config.customOpts), ",\n\n  //\n  // =====\n  // Hooks\n  // =====\n  // WebdriverIO provides several hooks you can use to interfere with the test process in order to enhance\n  // it and to build services around it. You can either apply a single function or an array of\n  // methods to it. If one of them returns with a promise, WebdriverIO will wait until that promise got\n  // resolved to continue.\n  /**\n   * Gets executed once before all workers get launched.\n   * @param {Object} config wdio configuration object\n   * @param {Array.<Object>} capabilities list of capabilities details\n   */\n  onPrepare: function(config, capabilities) {\n  },\n  /**\n   * Gets executed just before initialising the webdriver session and test framework. It allows you\n   * to manipulate configurations depending on the capability or spec.\n   * @param {Object} config wdio configuration object\n   * @param {Array.<Object>} capabilities list of capabilities details\n   * @param {Array.<String>} specs List of spec file paths that are to be run\n   */\n  // beforeSession: function(config, capabilities, specs) {\n  // },\n  /**\n   * Gets executed before test execution begins. At this point you can access to all global\n   * variables like 'browser'. It is the perfect place to define custom commands.\n   * @param {Array.<Object>} capabilities list of capabilities details\n   * @param {Array.<String>} specs List of spec file paths that are to be run\n   */\n  before: function(capabilities, specs) {\n    global.currentPage = {};\n\n    browser.addCommand('setPage', function (page) {\n        return global.currentPage = page;\n    })\n\n    ").concat(config.beforeHookPath ? "\n    const beforeHookFunc = require('".concat(config.beforeHookPath, "');\n    beforeHookFunc(capabilities, specs);\n    ") : "", "\n  },\n  /**\n   * Runs before a WebdriverIO command gets executed.\n   * @param {String} commandName hook command name\n   * @param {Array} args arguments that command would receive\n   */\n  // beforeCommand: function(commandName, args) {\n  // },\n  /**\n   * Runs before a Cucumber feature\n   * @param {Object} feature feature details\n   */\n  beforeFeature: function(uri, feature, scenarios) {\n    ").concat(config.beforeFeatureHookPath ? "\n    const beforeFeatureHookFunc = require('".concat(config.beforeFeatureHookPath, "');\n    beforeFeatureHookFunc(uri, feature, scenarios);\n    ") : "", "\n  },\n  /**\n   * Runs before a Cucumber scenario\n   * @param {Object} scenario scenario details\n   */\n  beforeScenario: function(uri, feature, scenario, sourceLocation) {\n    ").concat(config.beforeScenarioHookPath ? "\n    const beforeScenarioHookFunc = require('".concat(config.beforeScenarioHookPath, "');\n    beforeScenarioHookFunc(uri, feature, scenario, sourceLocation);\n    ") : "", "\n  },\n  /**\n   * Runs before a Cucumber step\n   * @param {Object} step step details\n   */\n  beforeStep: function(uri, feature, scenario, step) {\n  },\n  /**\n   * Runs after a Cucumber step\n   * @param {Object} stepResult step result\n   */\n  afterStep: function(uri, feature, { error, result }) {\n  },\n  /**\n   * Runs after a Cucumber scenario\n   * @param {Object} scenario scenario details\n   */\n  afterScenario: function(uri, feature, scenario, result, sourceLocation) {\n    ").concat(config.afterScenarioHookPath ? "\n    const afterScenarioHookFunc = require('".concat(config.afterScenarioHookPath, "');\n    afterScenarioHookFunc(uri, feature, scenario, result, sourceLocation);\n    ") : "", "\n  },\n  /**\n   * Runs after a Cucumber feature\n   * @param {Object} feature feature details\n   */\n  afterFeature: function(uri, feature, scenarios) {\n    ").concat(config.afterFeatureHookPath ? "\n    const afterFeatureHookFunc = require('".concat(config.afterFeatureHookPath, "');\n    afterFeatureHookFunc(uri, feature, scenarios);\n    ") : "", "\n  },\n\n  /**\n   * Runs after a WebdriverIO command gets executed\n   * @param {String} commandName hook command name\n   * @param {Array} args arguments that command would receive\n   * @param {Number} result 0 - command success, 1 - command error\n   * @param {Object} error error object if any\n   */\n  // afterCommand: function(commandName, args, result, error) {\n  // },\n  /**\n   * Gets executed after all tests are done. You still have access to all global variables from\n   * the test.\n   * @param {Number} result 0 - test pass, 1 - test fail\n   * @param {Array.<Object>} capabilities list of capabilities details\n   * @param {Array.<String>} specs List of spec file paths that ran\n   */\n  after: function(result, capabilities, specs) {\n    browser.pause(3000);\n    ").concat(config.afterHookPath ? "\n    const afterHookFunc = require('".concat(config.afterHookPath, "');\n    afterHookFunc(result, capabilities, specs);\n    ") : "", "\n  },\n  /**\n   * Gets executed right after terminating the webdriver session.\n   * @param {Object} config wdio configuration object\n   * @param {Array.<Object>} capabilities list of capabilities details\n   * @param {Array.<String>} specs List of spec file paths that ran\n   */\n  afterSession: function(config, capabilities, specs) {\n    // Workaround to make sure the chromedriver shuts down\n    // https://github.com/webdriverio-boneyard/wdio-selenium-standalone-service/issues/28\n    browser.pause(2000);\n  },\n  /**\n   * Gets executed after all workers got shut down and the process is about to exit. An error\n   * thrown in the onComplete hook will result in the test run failing.\n   * @param {Object} exitCode 0 - success, 1 - fail\n   * @param {Object} config wdio configuration object\n   * @param {Array.<Object>} capabilities list of capabilities details\n   * @param {<Object>} results object containing test results\n   */\n  //onComplete: function(exitCode, config, capabilities, results) {\n  //},\n  /**\n   * Gets executed when a refresh happens.\n   * @param {String} oldSessionId session ID of the old session\n   * @param {String} newSessionId session ID of the new session\n   */\n  //onReload: function(oldSessionId, newSessionId) {\n  //}\n};\n"); // Delete existing config (if exists) and create new config file

  var exists = _fs["default"].existsSync(wdioConfigFile);

  if (exists) {
    logger.verbose("create-wdio-config: %s already exists. deleting it", wdioConfigFile);

    _fs["default"].unlinkSync(wdioConfigFile);
  }

  logger.info("create-wdio-config: creating %s", wdioConfigFile);

  _fs["default"].writeFileSync(wdioConfigFile, fileOut);
}

;