"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _default = {
  common: {
    level: "info",
    levels: ["error", "warn", "info", "verbose", "debug", "silly"],
    verboseLevelIndex: 3
  },
  wdio: {
    levels: ["error", "warn", "info", "debug", "trace", "silent"]
  },
  console: {
    active: true
  },
  file: {
    active: false,
    filename: "default-logger.log"
  }
};
exports["default"] = _default;