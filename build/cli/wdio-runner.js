"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _cli = _interopRequireDefault(require("@wdio/cli"));

var _loggerModule = _interopRequireDefault(require("./loggerModule"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var logger = _loggerModule["default"].getLogger();

var _default = function _default(wdioConfigFile) {
  var wdio = new _cli["default"](wdioConfigFile, {});
  logger.info("wdio-runner: new launcher initalized");
  logger.info("wdio-runner: run");
  wdio.run().then(function (code) {
    logger.info("wdio-runner: finished");
    process.exit(code);
  }, function (error) {
    logger.info("wdio-runner: error");
    process.exit(1);
    throw new Error("Launcher failed to start the test: ".concat(error.stacktrace));
  });
};

exports["default"] = _default;