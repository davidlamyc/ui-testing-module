"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _winston = _interopRequireDefault(require("winston"));

var _loggerDefault = _interopRequireDefault(require("./logger-default.config"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = function () {
  var logger;
  var initialized = false;

  var format = _winston["default"].format.printf(function (nfo) {
    return "".concat(nfo.timestamp, " ").concat(nfo.level, " ").concat(nfo.message);
  });
  /**
   * Setup default logger
   */


  function setupDefaultLogger() {
    logger = _winston["default"].createLogger({
      level: _loggerDefault["default"].common.level,
      format: _winston["default"].format.combine(_winston["default"].format.splat(), _winston["default"].format.timestamp(), _winston["default"].format(function (info) {
        info.level = info.level.toUpperCase();
        return info;
      })())
    }); // Log to console

    if (_loggerDefault["default"].console && _loggerDefault["default"].console.active) {
      logger.add(new _winston["default"].transports.Console({
        format: _winston["default"].format.combine(_winston["default"].format.colorize(), format)
      }));
      logger.verbose("logger (default): Logging to console");
    } // Log to file


    if (_loggerDefault["default"].file && _loggerDefault["default"].file.active) {
      logger.add(new _winston["default"].transports.File({
        filename: _loggerDefault["default"].file.filename,
        format: _winston["default"].format.combine(format)
      }));
      logger.verbose("logger (default): Logging to file %s", _loggerDefault["default"].file.filename);
    }

    logger.verbose("logger (default): Logging level: %s", logger.level);
  }
  /**
   * Override default logger configuration
   * @param config the testconfig file
   */


  function overrideDefaultLogger(config) {
    // Override logToFile
    if (config.logToFile) {
      // Remove existing file transports if any
      logger.transports.filter(function (transport) {
        return transport instanceof _winston["default"].transports.File;
      }).map(function (transport) {
        return transport;
      }).forEach(function (transport) {
        logger.verbose("logger (override): Removing file transport: %s", transport.filename);
        logger.remove(transport);
      }); // Add file transport

      logger.add(new _winston["default"].transports.File({
        filename: config.logToFile,
        format: _winston["default"].format.combine(format)
      }));
      logger.info("logger (override): Logging to file: %s", config.logToFile);
    } // Override logLevel


    if (config.logLevel) {
      if (_loggerDefault["default"].common.levels.includes(config.logLevel)) {
        logger.info("logger (override): Logging level: %s", config.logLevel);
        logger.level = config.logLevel;
      } else if (_loggerDefault["default"].wdio.levels.includes(config.logLevel)) {
        logger.info("logger (override): Logging level: %s", config.logLevel);
        logger.silent = config.logLevel === "silent";
        logger.level = config.logLevel;

        _loggerDefault["default"].wdio.levels.forEach(function (v, i) {
          return logger.levels[v] = i;
        });
      } else {
        logger.warn("logger (override): Logging level [%s] is not recognized.", config.logLevel);
      }
    }

    logger.info("logger (override): finished override");
  }
  /**
   * Init logger
   */


  function init() {
    setupDefaultLogger();
    logger.verbose("logger (default): finished initialization");
  }

  function getLogger() {
    if (!initialized) {
      init();
      initialized = true;
    }

    return logger;
  }

  return {
    getLogger: getLogger,
    init: init,
    overrideDefaultLogger: overrideDefaultLogger
  };
}();

exports["default"] = _default;