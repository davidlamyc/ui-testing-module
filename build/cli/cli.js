"use strict";

var _figlet = _interopRequireDefault(require("figlet"));

var _fs = _interopRequireDefault(require("fs"));

var _path = _interopRequireDefault(require("path"));

var _yargs = _interopRequireDefault(require("yargs"));

var _cliConfig = _interopRequireDefault(require("./cli-config"));

var _createWdioConfig = _interopRequireDefault(require("./create-wdio-config"));

var _loggerModule = _interopRequireDefault(require("./loggerModule"));

var _defaultTest = _interopRequireDefault(require("./default-test.config"));

var _wdioRunner = _interopRequireDefault(require("./wdio-runner"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var DEFAULT_OVERRIDE_CONFIG_PATH = "./test/config.json";
var CLI_OPTIONS = ["baseUrl", "host", "hrsave", "path", "port", "specs", "tags", "user", "key", "bsid", "vrsave"];

var logger = _loggerModule["default"].getLogger();
/*
 * Parse CLI arguments and create override object for launcher
 */


var usageMsg = "\n==========================\nUI Test Infrastructure\n==========================\nUsage: ui-testing-module\nUsage: ui-testing-module config\n\nConfig file defaults to '<PLACEHOLDER: INSERT DEFAULT PATH HERE>'\nwdio.conf.js will be created/overwritten whenever the test runner is run. Do not manually edit this file.\nThe [options] object will override all options from the config file and in wdio.conf.js.\n\nFor full instructions on setup and usage, please read the documentation at\n<REPLACE PLACEHOLDER>\n";
var options = {
  baseUrl: {
    alias: "b",
    describe: "Shortern URL command calls by setting a base URL"
  },
  // bsid: {
  //   describe: "The browserstack identifier"
  // },
  help: {
    describe: "prints digcore-fe-test help menu"
  },
  host: {
    alias: "h",
    describe: "Selenium server host address"
  },
  hrsave: {
    describe: "Save HTTP Responses, skip comparison"
  },
  // key: {
  //   describe: "The browserstack key"
  // },
  maxInstances: {
    describe: "Maximum number of concurrent driver instances"
  },
  path: {
    describe: "Selenium server path (default: /wd/hub)"
  },
  port: {
    alias: "p",
    describe: "Selenium server port"
  },
  specs: {
    describe: "Run only the feature(s) specified (accepts glob)"
  },
  tags: {
    describe: "To run only the tests with the specified tag(s)"
  },
  user: {
    describe: "The browserstack user"
  },
  vrsave: {
    describe: "Save Visual Regression Snapshots, skip comparison"
  }
};
/*
 * Parse cli perimeters with yargs.
 * First perimeter is the relative path to project config file.
 */

var args = _yargs["default"].usage(usageMsg).option(options).help().argv;
/*
 * If first arg provided is 'config', run config tool.
 * If not, generate wdio config file and run runner with cli args
 */


switch (args._[0]) {
  case "config":
    (0, _cliConfig["default"])();
    break;

  default:
    /*
     * Parse path to config override file
     */
    logger.info('Starting task...');
    logger.info('Process running in:', process.cwd());
    logger.info('Current directory:', __dirname);

    if (!args._[0]) {
      logger.info("cli: No config file specified! Looking for config file at default path.");
    } else if (!_fs["default"].existsSync(args._[0])) {
      logger.info("cli: Config file at %s not found! Looking for config file at default path. %s", args._[0]);
    }

    var overrideFilePath = _fs["default"].existsSync(args._[0]) ? args._[0] : DEFAULT_OVERRIDE_CONFIG_PATH;
    logger.info("cli: Looking for config file at %s", overrideFilePath);
    /*
     * Set first argument to "config" if no default config file is found or path is specified
     * If config file exists, load it up
     */

    var overrideOpts;

    if (!_fs["default"].existsSync(overrideFilePath)) {
      logger.error("cli: No config file found, running configuration tool");
      (0, _cliConfig["default"])();
    } else {
      try {
        overrideOpts = JSON.parse(_fs["default"].readFileSync(overrideFilePath, {
          encoding: "utf8"
        }));
        Object.assign(_defaultTest["default"], overrideOpts);
        logger.info("cli: Successfully imported config file %s", overrideFilePath);
      } catch (err) {
        logger.error("cli: Unable to import config file at %s", overrideFilePath);
      }
    }
    /**
     * Parse CLI arguments, and use this to override default & app config keys)
     */


    CLI_OPTIONS.forEach(function (key) {
      if (args[key]) {
        _defaultTest["default"][key] = key === "specs" ? [args[key]] : args[key];
        var printableKey = key !== "key" && key !== "user" ? args[key] : "##############";
        logger.info("cli: CLI override of %s (new value: %s)", key, printableKey);
      }
    });

    _loggerModule["default"].overrideDefaultLogger(_defaultTest["default"]);
    /**
     * Launch wdio
     */


    var configDir = _path["default"].dirname(overrideFilePath);

    var wdioConfigFile = _path["default"].resolve(configDir, "./wdio.conf.js");

    (0, _createWdioConfig["default"])(_defaultTest["default"], configDir, wdioConfigFile);
    console.log(_figlet["default"].textSync("STAIZEN-FE-TEST", {
      font: "Standard"
    }));
    (0, _wdioRunner["default"])(wdioConfigFile);
}